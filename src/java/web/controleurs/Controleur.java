/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.controleurs;

import entites.Region;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Administrateur
 */
@ManagedBean
@ViewScoped
public class Controleur {
    
   private String codereg;
    
   private Region reg;
    
    public void recherche(){
        
        reg=Dao.getRegionDeCode(codereg);
    }

    public String getCodereg() {
        return codereg;
    }

    public void setCodereg(String codereg) {
        this.codereg = codereg;
    }

    public Region getReg() {
        return reg;
    }

    public void setReg(Region reg) {
        this.reg = reg;
    }
    
    
}
